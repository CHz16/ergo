// game.js
// Copyright (c) 2012 'Ili Butterfield. No permission is granted to copy,
// modify, or distribute this code in full or in part without prior written
// permission from the author.
//
// Ergo main game code.


//
// CLASSES
//

function Button(unselectedImg, selectedImg, left, top, action) {
    this.unselectedImg = unselectedImg;
    this.selectedImg = selectedImg;
    this.left = left;
    this.right = left + unselectedImg.width - 1;
    this.top = top;
    this.bottom = top + unselectedImg.height - 1;
    this.action = action;
}

Button.prototype.draw = function(mouseX, mouseY) {
    if (loadingThings) return;

    if (this.mouseWithin(mouseX, mouseY)) {
        ctx.drawImage(this.selectedImg, this.left, this.top);
    } else {
        ctx.drawImage(this.unselectedImg, this.left, this.top);
    }
}

Button.prototype.drawUnselected = function() {
    ctx.drawImage(this.unselectedImg, this.left, this.top);
}

Button.prototype.click = function(mouseX, mouseY) {
    if (loadingThings) return;
    if (this.mouseWithin(mouseX, mouseY)) this.action();
}


Button.prototype.mouseWithin = function(x, y) {
    return (this.left <= x && this.right >= x && this.top <= y && this.bottom >= y);
}

//
// GAME PARAMETERS
//

var gameWidth = 527, gameHeight = 385;
var tileSize = 35;
var miniTileSize = 14;
var gridSize = 9;
var accessListSize = 10;
var settingsSize = 15;
var defaultAnimationSpeed = 4;
var scrambleAnimationSpeeds = [10, 20, 100];
var scrambleAnimationCutoffs = [50, 100];
var highlightSpeed = 500;
var accessListBorderColor = "#FF0000";
var accessListColors = ["#000000", "#808080", "#FFFFFF"];
var accessListHighlights = ["#000000", "#800000", "#FF0000"];
var loadingMessageWidth = 350, loadingMessageHeight = 30, loadingMessageHeightOffset = 10;
var shiftSoundMinimumDuration = 50;


//
// GLOBALS
//

var musicPlayer, sfxPlayer; // Audio players
var canvas, ctx; // canvas and its drawing context
var canvasX, canvasY; // the location of the top-left pixel of the canvas
var buttons = [];
var tilesets = ["alien", "happyland", "ludyland", "pierre", "planet", "plateau"];
var selector;
var tiles = [], miniTiles = [], arrows = [];
var frame;
var deactivatedButtons, actionGenerator;
var arrowButtons = [];
var grid, goal;
var shiftQueue;
var accessList;
var currentTileset;
var selectedPattern = 0, selectedRuleset = 0;
var currentPattern, currentRuleset, highlight = false;
var musicImages = [], musicOn = false;
var sfxImages = [], sfxOn = true, sfxContext;
var shiftSoundLastPlayed = 0;
var accessListImages = []
var animationSpeed;
var clear = 0;
var loadingThings = false;


//
// INITIALIZATION
//


window.onload = init;
function init() {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    console.log(canvasThings);
    canvas = canvasThings.canvas;
    ctx = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.getElementById("canvas").parentNode.replaceChild(canvas, document.getElementById("canvas"));
    canvas.id = "canvas";
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";
    findCanvasPosition();

    // Prevents the gray highlighting of the canvas object. The explicit touch
    // handling should obviate the need for this but I'll leave it in anyway.
    canvas.style.webkitTapHighlightColor = "transparent";

    initializeAccessList();

    if (getCookie("ergo_sfx") == "off") {
        sfxOn = false;
    }

    animationSpeed = defaultAnimationSpeed;

    canvas.onmouseup = function(evt) {
        canvas.onmouseup = undefined;

        // Safari 12 doesn't like it if we try to set up audio in a resource
        // load callback, so we do it specifically here in the click handler
        if (getCookie("ergo_music") == "on") {
            loadPlayer();
            musicOn = true;
        }
        sfxContext = new (window.AudioContext || window.webkitAudioContext)();
        sfxContext.resume();

        var savedTileset = getCookie("ergo_tileset");
        if (savedTileset !== null) {
            loadTileset(savedTileset, loadUIImages);
        } else {
            loadTileset("alien", loadUIImages)
        }
    };
    drawLoadingMessage("click to play");
}

function init2() {
    createSettingsButtons();
    createTilesetSelector();

    // The first puzzle is always loaded in Cogito with these two colors and
    // 30 moves to scramble instead of the usual 35.
    loadPuzzle([1, 5], 30);

    setTimeout(highlightCurrentPuzzle, highlightSpeed);

    canvas.onmousemove = handleMouseMove;
    canvas.onmouseup = handleMouseUp;
    // Redraw the buttons when the mouse leaves the canvas to make sure they
    // all get deselected. This catches the case when a button is at the edge
    // of the canvas and the mouse moves from it off the canvas.
    canvas.onmouseout = handleMouseMove;

    // Eat this event to prevent rapid clicking from selecting text elsewhere.
    canvas.onmousedown = function() { return false; };

    // Extremely simplistic tap handling
    canvas.ontouchend = function(evt) {
        // Takes the pageX and pageY of the first touch of a touch event and
        // sets the event's top level pageX and pageY properties to them so it
        // can be passed to functions expecting mouse events. THIS IS
        // HILARIOUSLY UNSAFE BECAUSE IT PLUGS ITS EARS AND PRETENDS THAT
        // MULTITOUCH EVENTS DON'T EXIST HA HA
        evt.pageX = evt.changedTouches[0].pageX;
        evt.pageY = evt.changedTouches[0].pageY;

        handleMouseMove(evt); // Turn the button on
        window.setTimeout(drawButtons, 50); // Turn the button off later

        handleMouseUp(evt);
        return false;
    };
    canvas.ontouchcancel = function(evt) { drawButtons(); return false; };

    document.body.onresize = handleWindowResize;
}

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

function findCanvasPosition() {
    canvasX = canvas.offsetLeft;
    canvasY = canvas.offsetTop;

    var obj = canvas;
    while (obj = obj.offsetParent) {
        canvasX += obj.offsetLeft;
        canvasY += obj.offsetTop;
    }
}

function initializeAccessList() {
    var savedAccessList = getCookie("ergo_progress");

    accessList = [];
    for (var row = 0; row < patterns.length; row++) {
        accessList[row] = [];
        for (var col = 0; col < rulesets.length; col++) {
            // Multiply by 1 to convert from string to integer.
            accessList[row][col] = savedAccessList ? savedAccessList.charAt(row * rulesets.length + col) * 1 : 0;
        }
    }

    if (!savedAccessList) {
        accessList[0][0] = 1;
    }
}

function loadTileset(tileset, followup) {
    loadingThings = true;
    currentTileset = tileset;

    var urls = ["tile0.png", "tile0mini.png", "tile1.png", "tile1mini.png", "tile2.png", "tile2mini.png", "tile3.png", "tile3mini.png", "tile4.png", "tile4mini.png", "tile5.png", "tile5mini.png", "leftunselected.png", "leftselected.png", "bottomunselected.png", "bottomselected.png", "rightunselected.png", "rightselected.png", "topunselected.png", "topselected.png", "frame.png"];
    var base = "images/tilesets/" + currentTileset + "/";
    var loadCallback = function(preloader, index) {
        drawLoadingMessage("tileset images loaded: " + (index + 1) + "/" + urls.length);

        if (index == (preloader.urls.length - 1)) {
            tiles = [];
            miniTiles = [];
            for (var i = 0; i < 6; i++) {
                tiles.push(preloader.getImage("tile" + i + ".png"));
                miniTiles.push(preloader.getImage("tile" + i + "mini.png"));
            }

            arrows = [];
            arrows.push(preloader.getImage("leftunselected.png"));
            arrows.push(preloader.getImage("leftselected.png"));
            arrows.push(preloader.getImage("bottomunselected.png"));
            arrows.push(preloader.getImage("bottomselected.png"));
            arrows.push(preloader.getImage("rightunselected.png"));
            arrows.push(preloader.getImage("rightselected.png"));
            arrows.push(preloader.getImage("topunselected.png"));
            arrows.push(preloader.getImage("topselected.png"));

            frame = preloader.getImage("frame.png");

            loadingThings = false;
            followup();
        }
    };
    var errorCallback = function(preloader, index) {
        drawLoadingMessage("error loading image: " + currentTileset + " - " + urls[index]);
        return true;
    };

    var preloader = new ImagePreloader(urls, {base: base, loadCallback: loadCallback, errorCallback: errorCallback});
}

function loadPuzzle(substitution, scrambleAmount) {
    if (!accessList[selectedPattern][selectedRuleset]) return;

    deactivatedButtons = rulesets[selectedRuleset].deactivatedButtons ? rulesets[selectedRuleset].deactivatedButtons : [];
    actionGenerator = rulesets[selectedRuleset].actionGenerator;

    if (arrowButtons.length) removeArrowButtons();
    arrowButtons = [];
    createArrowButtons();

    if (!substitution) substitution = generateSubstitution();
    if (patterns[selectedPattern].substitution) substitution = patterns[selectedPattern].substitution;

    if (!scrambleAmount) scrambleAmount = rulesets[selectedRuleset].scrambleAmount;
    var scrambleLevel = 0;
    while (scrambleLevel <= scrambleAnimationCutoffs.length && scrambleAmount > scrambleAnimationCutoffs[scrambleLevel]) {
        scrambleLevel++;
    }
    animationSpeed = scrambleAnimationSpeeds[scrambleLevel];

    if (patterns[selectedPattern].layoutGenerator) layout = patterns[selectedPattern].layoutGenerator();
    else layout = patterns[selectedPattern].layout;

    grid = patternSubstitute(layout, substitution);
    goal = gridCopy(grid);

    clear += 1;
    shiftQueue = [];
    for (var i = 0; i < scrambleAmount; i++) {
        generateRandomArrowPress();
    }

    currentPattern = selectedPattern;
    currentRuleset = selectedRuleset;

    fullRedraw();
    setTimeout(beginShifting, 1000);
}

function generateSubstitution() {
    var substitution = [0, 1, 2, 3, 4, 5];

    for (var i = 0; i < substitution.length - 1; i++) {
        var temp = substitution[i];
        var swapIndex = randRange(i, substitution.length - 1);
        substitution[i] = substitution[swapIndex];
        substitution[swapIndex] = temp;
    }

    return substitution;
}

function patternSubstitute(pattern, substitution) {
    var newPattern = [];

    for (var row = 0; row < pattern.length; row++) {
        newPattern[row] = [];
        for (var col = 0; col < pattern[row].length; col++) {
            newPattern[row][col] = substitution[pattern[row][col]];
        }
    }

    return newPattern;
}

function gridCopy(grid) {
    var newGrid = [];
    for (var row = 0; row < grid.length; row++) {
        newGrid[row] = [];
        for (var col = 0; col < grid[row].length; col++) {
            newGrid[row][col] = grid[row][col];
        }
    }
    return newGrid;
}

function removeArrowButtons() {
    arrowButtons.forEach(function(button) { removeButton(button); });
}

function createArrowButtons() {
    // left side
    for (var button = 0; button < gridSize; button ++) {
        if (buttonDeactivated(0, button)) continue;
        arrowButtons[arrowButtons.length] = makeButton(arrows[0], arrows[1], 0, (button + 1) * tileSize, pressArrow(0, button));
    }

    // bottom side
    for (var button = 0; button < gridSize; button ++) {
        if (buttonDeactivated(1, button)) continue;
        arrowButtons[arrowButtons.length] = makeButton(arrows[2], arrows[3], (button + 1) * tileSize, (gridSize + 1) * tileSize, pressArrow(1, button));
    }

    // right side
    for (var button = 0; button < gridSize; button ++) {
        if (buttonDeactivated(2, button)) continue;
        arrowButtons[arrowButtons.length] = makeButton(arrows[4], arrows[5], (gridSize + 1) * tileSize, (gridSize - button) * tileSize, pressArrow(2, button));
    }

    // top side
    for (var button = 0; button < gridSize; button ++) {
        if (buttonDeactivated(3, button)) continue;
        arrowButtons[arrowButtons.length] = makeButton(arrows[6], arrows[7], (gridSize - button) * tileSize, 0, pressArrow(3, button));
    }
}

function buttonDeactivated(side, arrow) {
    for (var i = 0; i < deactivatedButtons.length; i++) {
        var testButton = deactivatedButtons[i];
        if ((testButton[0] == side) && (testButton[1] == arrow)) return true;
    }
    return false;
}

function loadUIImages() {
    loadingThings = true;

    var urls = ["leftsettingunselected.png", "leftsettingselected.png", "upsettingunselected.png", "upsettingselected.png", "rightsettingunselected.png", "rightsettingselected.png", "downsettingunselected.png", "downsettingselected.png", "loadunselected.png", "loadselected.png", "musicoffunselected.png", "musiconunselected.png", "musicoffselected.png", "musiconselected.png", "sfxoffunselected.png", "sfxonunselected.png", "sfxoffselected.png", "sfxonselected.png"];
    var base = "images/common/";
    var loadCallback = function(preloader, index) {
        drawLoadingMessage("UI images loaded: " + (index + 1) + "/" + urls.length);

        if (index == (preloader.urls.length - 1)) {
            accessListImages.push(preloader.getImage("leftsettingunselected.png"));
            accessListImages.push(preloader.getImage("leftsettingselected.png"));
            accessListImages.push(preloader.getImage("upsettingunselected.png"));
            accessListImages.push(preloader.getImage("upsettingselected.png"));
            accessListImages.push(preloader.getImage("rightsettingunselected.png"));
            accessListImages.push(preloader.getImage("rightsettingselected.png"));
            accessListImages.push(preloader.getImage("downsettingunselected.png"));
            accessListImages.push(preloader.getImage("downsettingselected.png"));
            accessListImages.push(preloader.getImage("loadunselected.png"));
            accessListImages.push(preloader.getImage("loadselected.png"));

            musicImages.push(preloader.getImage("musicoffunselected.png"));
            musicImages.push(preloader.getImage("musicoffselected.png"));
            musicImages.push(preloader.getImage("musiconunselected.png"));
            musicImages.push(preloader.getImage("musiconselected.png"));

            sfxImages.push(preloader.getImage("sfxoffunselected.png"));
            sfxImages.push(preloader.getImage("sfxoffselected.png"));
            sfxImages.push(preloader.getImage("sfxonunselected.png"));
            sfxImages.push(preloader.getImage("sfxonselected.png"));

            loadingThings = false;
            if (sfxOn) {
                loadSounds(init2);
            } else {
                init2();
            }
        }
    };
    var errorCallback = function(preloader, index) {
        drawLoadingMessage("error loading image: " + urls[index]);
        return true;
    };

    var preloader = new ImagePreloader(urls, {base: base, loadCallback: loadCallback, errorCallback: errorCallback});
}

function loadSounds(followup) {
    loadingThings = true;

    var sounds = [
        {key: "shift", urls: ["shift.ogg", "shift.wav"]},
        {key: "fanfare", urls: ["fanfare.ogg", "fanfare.wav"]}
    ];
    var base = "audio/";
    var loadCallback = function(player, index) {
        drawLoadingMessage("sounds loaded: " + (index + 1) + "/" + sounds.length);

        if (index == (sfxPlayer.sounds.length - 1)) {
            loadingThings = false;
            followup();
        }
    };
    var errorCallback = function(player, index) {
        drawLoadingMessage("error loading sound: " + sounds[index].key);
        return true;
    };

    sfxPlayer = new SoundEffectPlayer(sounds, {context: sfxContext, base: base, minimumDuration: shiftSoundMinimumDuration, loadCallback: loadCallback, errorCallback: errorCallback});
}

function createSettingsButtons() {
    // level select
    var left = (gridSize + 2) * tileSize + miniTileSize;
    var top = (gridSize + 2) * tileSize - (patterns.length * accessListSize) - miniTileSize - 3 * settingsSize - 1;
    makeButton(accessListImages[0], accessListImages[1], left, top + settingsSize, adjustPuzzle(0, -1));
    makeButton(accessListImages[2], accessListImages[3], left + settingsSize, top, adjustPuzzle(-1, 0));
    makeButton(accessListImages[4], accessListImages[5], left + 2 * settingsSize, top + settingsSize, adjustPuzzle(0, 1));
    makeButton(accessListImages[6], accessListImages[7], left + settingsSize, top + 2 * settingsSize, adjustPuzzle(1, 0));
    makeButton(accessListImages[8], accessListImages[9], left + 3 * settingsSize + miniTileSize, top + settingsSize, loadPuzzle);

    // music
    left = (gridSize + 2) * tileSize + miniTileSize;
    top = (gridSize + 1) * miniTileSize;
    makeButton(musicImages[musicOn ? 2 : 0], musicImages[musicOn ? 3 : 1], left, top, toggleMusic);

    // SFX
    left = (gridSize + 2) * tileSize + miniTileSize * 3;
    top = (gridSize + 1) * miniTileSize;
    makeButton(sfxImages[sfxOn ? 2 : 0], sfxImages[sfxOn ? 3 : 1], left, top, toggleSfx);
}

function createTilesetSelector() {
    selector = document.createElement("select");
    selector.style.position = "absolute";
    positionSelector();

    for (var i = 0; i < tilesets.length; i++) {
        var option = document.createElement("option");
        option.text = tilesets[i];
        if (tilesets[i] == currentTileset) {
            option.selected = "selected";
        }
        selector.appendChild(option);
    }

    document.body.appendChild(selector);
    positionSelector();
    selector.onchange = function() {
        loadTileset(this.value, function() {
            removeArrowButtons();
            createArrowButtons();
            fullRedraw();
            setCookie("ergo_tileset", currentTileset, 10 * 365 * 24 * 60 * 60 * 1000);
        });
    };
}

function positionSelector() {
    selector.style.left = (canvasX + (gridSize + 2) * tileSize + miniTileSize * 5) + "px";
    selector.style.top = (canvasY + (gridSize + 1) * miniTileSize - 3) + "px"; // fudge factor
}

function adjustPuzzle(patternChange, rulesetChange) {
    return function() {
        selectedPattern = (selectedPattern + patternChange + patterns.length) % patterns.length;
        selectedRuleset = (selectedRuleset + rulesetChange + rulesets.length) % rulesets.length;
        drawAccessList();
    };
}

function toggleMusic() {
    if (musicPlayer && musicOn) musicPlayer.pause();
    else if (musicPlayer && !musicOn) musicPlayer.play();
    else loadPlayer();

    musicOn = !musicOn;
    var index = musicOn ? 2 : 0;
    this.unselectedImg = musicImages[index];
    this.selectedImg = musicImages[index + 1];
    this.draw(this.left, this.top);

    // iOS devices won't play the music without user input, so we won't save the
    // music setting cookie for them. This means music will always start off,
    // forcing manual activation (which is what we want).
    var iosUserAgent = navigator.userAgent.indexOf("iPad") != -1 || navigator.userAgent.indexOf("iPhone") != -1 || navigator.userAgent.indexOf("iPod") != -1;

    if (window.MSStream || !iosUserAgent) {
        if (musicOn) setCookie("ergo_music", "on", 10 * 365 * 24 * 60 * 60 * 1000);
        else setCookie("ergo_music", "off", 10 * 365 * 24 * 60 * 60 * 1000);
    }
}

function toggleSfx() {
    if (!sfxOn && sfxPlayer === undefined) {
        var boundToggleSfx = toggleSfx.bind(this);
        loadSounds(function() { boundToggleSfx(); fullRedraw(); });
        return;
    }

    if (sfxOn) {
        sfxPlayer.stopAllSounds();
    }

    sfxOn = !sfxOn;
    var index = sfxOn ? 2 : 0;
    this.unselectedImg = sfxImages[index];
    this.selectedImg = sfxImages[index + 1];
    this.draw(this.left, this.top);

    if (sfxOn) setCookie("ergo_sfx", "on", 10 * 365 * 24 * 60 * 60 * 1000);
    else setCookie("ergo_sfx", "off", 10 * 365 * 24 * 60 * 60 * 1000);
}

function pressArrow(side, arrow) {
    return function() {
        if (!shiftQueue.length) {
            makeShifts(side, arrow);
            beginShifting();
        }
    };
}

function generateRandomArrowPress() {
    var side, arrow;

    do {
        side = randRange(0, 3);
        arrow = randRange(0, gridSize);
    } while (buttonDeactivated(side, arrow));

    makeShifts(side, arrow);
}

function randRange(min, max) {
    return min + Math.floor((max - min + 1) * Math.random());
}

function makeShifts(side, arrow) {
    actionGenerator(side, arrow).forEach(makeShift);
}

function makeShift(action) {
    var shiftSide = (action[0] + 4) % 4;
    var shiftArrow = (action[1] + gridSize) % gridSize;
    var shiftAmount = action[2];

    if (shiftAmount < 0) {
        shiftSide = (shiftSide + 2) % 4;
        shiftArrow = gridSize - shiftArrow - 1;
        shiftAmount = -shiftAmount;
    }

    for (var i = 0; i < shiftAmount; i++) {
        shiftQueue[shiftQueue.length] = [shiftSide, shiftArrow];
    }
}

function checkForWin() {
    for (var row = 0; row < gridSize; row++) {
        for (var col = 0; col < gridSize; col++) {
            if (grid[row][col] != goal[row][col]) return false;
        }
    }
    return true;
}

function completePuzzle() {
    accessList[currentPattern][currentRuleset] = 2;
    if (currentPattern > 0 && !accessList[currentPattern - 1][currentRuleset]) {
        accessList[currentPattern - 1][currentRuleset] = 1;
    }
    if (currentPattern < (patterns.length - 1) && !accessList[currentPattern + 1][currentRuleset]) {
        accessList[currentPattern + 1][currentRuleset] = 1;
    }
    if (currentRuleset > 0 && !accessList[currentPattern][currentRuleset - 1]) {
        accessList[currentPattern][currentRuleset - 1] = 1;
    }
    if (currentRuleset < (rulesets.length - 1) && !accessList[currentPattern][currentRuleset + 1]) {
        accessList[currentPattern][currentRuleset + 1] = 1;
    }

    var savedAccessList = "";
    for (var row = 0; row < patterns.length; row++) {
        for (var col = 0; col < rulesets.length; col++) {
            savedAccessList += accessList[row][col].toString();
        }
    }
    setCookie("ergo_progress", savedAccessList, 10 * 365 * 24 * 60 * 60 * 1000);

    removeArrowButtons();
    fullRedraw();

    if (sfxOn) {
        sfxPlayer.playSound("fanfare");
    }
}

function beginShifting() {
    if (clear) clear -= 1;
    if (!clear) startShift();
}

function startShift() {
    if (clear) return;

    if (!shiftQueue.length) {
        animationSpeed = defaultAnimationSpeed;
        if (checkForWin()) completePuzzle();
        else shiftQueue = [];
    } else {
        shift(animationSpeed);
        if (sfxOn) {
            sfxPlayer.playSound("shift");
        }
    }
}

function shift(n) {
    if (!shiftQueue.length) return;
    if (clear) return;

    var side = shiftQueue[0][0];
    var arrow = shiftQueue[0][1];

    ctx.save();

    ctx.beginPath();
    ctx.moveTo(tileSize, tileSize);
    ctx.lineTo(tileSize, (gridSize + 1) * tileSize);
    ctx.lineTo((gridSize + 1) * tileSize, (gridSize + 1) * tileSize);
    ctx.lineTo((gridSize + 1) * tileSize, tileSize);
    ctx.clip();

    n = Math.min(tileSize, n);

    if (side == 0) {
        // left
        for (var i = -1; i < gridSize; i++) {
            ctx.drawImage(tiles[grid[arrow][(i + gridSize) % gridSize]], (i + 1) * tileSize + n, (arrow + 1) * tileSize);
        }
    } else if (side == 1) {
        // bottom
        for (var i = 0; i <= gridSize; i++) {
            ctx.drawImage(tiles[grid[(i + gridSize) % gridSize][arrow]], (arrow + 1) * tileSize, (i + 1) * tileSize - n);
        }
    } else if (side == 2) {
        // right
        for (var i = 0; i <= gridSize; i++) {
            ctx.drawImage(tiles[grid[gridSize - arrow - 1][(i + gridSize) % gridSize]], (i + 1) * tileSize - n, (gridSize - arrow) * tileSize);
        }
    } else if (side == 3) {
        // top
        for (var i = -1; i < gridSize; i++) {
            ctx.drawImage(tiles[grid[(i + gridSize) % gridSize][gridSize - arrow - 1]], (gridSize - arrow) * tileSize, (i + 1) * tileSize + n);
        }
    }

    ctx.restore();

    if (n < tileSize) setTimeout(shift, 10, n + animationSpeed);
    else endShift();
}

function endShift() {
    if (clear) return;

    var side = shiftQueue[0][0];
    var arrow = shiftQueue[0][1];

    if (side == 0) {
        // left
        var temp = grid[arrow][gridSize - 1];
        for (var i = gridSize - 2; i >= 0; i--) {
            grid[arrow][i + 1] = grid[arrow][i];
        }
        grid[arrow][0] = temp;
    } else if (side == 1) {
        // bottom
        var temp = grid[0][arrow];
        for (var i = 0; i < (gridSize - 1); i++) {
            grid[i][arrow] = grid[i + 1][arrow];
        }
        grid[gridSize - 1][arrow] = temp;
    } else if (side == 2) {
        // right
        var temp = grid[gridSize - arrow - 1][0];
        for (var i = 0; i < (gridSize - 1); i++) {
            grid[gridSize - arrow - 1][i] = grid[gridSize - arrow - 1][i + 1];
        }
        grid[gridSize - arrow - 1][gridSize - 1] = temp;
    } else if (side == 3) {
        // top
        var temp = grid[gridSize - 1][gridSize - arrow - 1];
        for (var i = gridSize - 2; i >= 0; i--) {
            grid[i + 1][gridSize - arrow - 1] = grid[i][gridSize - arrow - 1];
        }
        grid[0][gridSize - arrow - 1] = temp;
    }

    shiftQueue.shift();
    startShift();
}

function makeButton(unselectedImg, selectedImg, left, top, action) {
    var button = new Button(unselectedImg, selectedImg, left, top, action);
    buttons[buttons.length] = button;
    return button;
}

function removeButton(button) {
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i] == button) delete buttons[i];
    }
}

function fullRedraw() {
    ctx.clearRect(0, 0, gameWidth, gameHeight);
    ctx.drawImage(frame, 0, 0);
    drawButtons();
    drawGrid();
    drawGoal();
    drawAccessList();
}

function drawButtons() {
    buttons.forEach(function(button) { button.drawUnselected(); });
}

function drawGrid() {
    for (var row = 0; row < gridSize; row++) {
        for (var col = 0; col < gridSize; col++) {
            ctx.drawImage(tiles[grid[row][col]], (col + 1) * tileSize, (row + 1) * tileSize);
        }
    }
}

function drawGoal() {
    ctx.strokeStyle = "#000000";
    ctx.lineWidth = 1.0;

    var left = (gridSize + 2) * tileSize + miniTileSize;
    var top = 0;

    ctx.strokeRect(left + .5, top + .5, gridSize * miniTileSize + 1, gridSize * miniTileSize + 1);

    for (var row = 0; row < gridSize; row++) {
        for (var col = 0; col < gridSize; col++) {
            ctx.drawImage(miniTiles[goal[row][col]], left + col * miniTileSize + 1, top + row * miniTileSize + 1);
        }
    }
}

function drawAccessList() {
    var left = (gridSize + 2) * tileSize + miniTileSize;
    var top = (gridSize + 2) * tileSize - (patterns.length * accessListSize) - 1;

    for (var row = 0; row < patterns.length; row++) {
        for (var col = 0; col < rulesets.length; col++) {
            ctx.fillStyle = accessListColors[accessList[row][col]];
            ctx.fillRect(left + col * accessListSize, top + row * accessListSize, accessListSize, accessListSize);
        }
    }

    ctx.strokeStyle = accessListBorderColor;
    ctx.strokeRect(left - .5, top - .5, accessListSize * rulesets.length + 1, accessListSize * patterns.length + 1);
    ctx.strokeRect(left + selectedRuleset * accessListSize + .5, top + selectedPattern * accessListSize + .5, accessListSize - 1, accessListSize - 1);
}

function drawLoadingMessage(message) {
    var bodyStyle = window.getComputedStyle(document.body, null);
    if (bodyStyle.fontSize && bodyStyle.fontFamily) {
        ctx.font = bodyStyle.fontSize + " " + bodyStyle.fontFamily;
    } else {
        ctx.font = "16px Palatino, 'Palatino Linotype', Georgia, serif";
    }

    ctx.strokeStyle = "#000000";
    ctx.lineWidth = 1.0;
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect((gameWidth - loadingMessageWidth) / 2, (gameHeight - 30) / 2, loadingMessageWidth, loadingMessageHeight);
    ctx.strokeRect((gameWidth - loadingMessageWidth) / 2, (gameHeight - 30) / 2, loadingMessageWidth, loadingMessageHeight);

    ctx.fillStyle = "#000000";
    var metrics = ctx.measureText(message);
    ctx.fillText(message, (gameWidth - metrics.width) / 2, (gameHeight + 30) / 2 - loadingMessageHeightOffset);
}

function loadPlayer() {
    musicPlayer = new Audio();
    musicPlayer.loop = "loop";

    if (musicPlayer.canPlayType("audio/mpeg") !== "" && musicPlayer.canPlayType("audio/mpeg") !== "no") {
        musicPlayer.src = "audio/serpents.mp3";
    } else {
        musicPlayer.src = "audio/serpents.ogg";
    }

    musicPlayer.load();
    musicPlayer.play();
}

function highlightCurrentPuzzle() {
    var left = (gridSize + 2) * tileSize + miniTileSize + 1;
    var top = (gridSize + 2) * tileSize - (patterns.length * accessListSize);

    ctx.fillStyle = (highlight) ? accessListHighlights[accessList[currentPattern][currentRuleset]] : accessListColors[accessList[currentPattern][currentRuleset]];
    ctx.fillRect(left + currentRuleset * accessListSize, top + currentPattern * accessListSize, accessListSize - 2, accessListSize - 2);

    highlight = !highlight;
    setTimeout(highlightCurrentPuzzle, highlightSpeed);
}

function handleMouseMove(evt) {
    buttons.forEach(function(button) { button.draw(evt.pageX - canvasX, evt.pageY - canvasY); });
}

function handleMouseUp(evt) {
    buttons.forEach(function(button) { button.click(evt.pageX - canvasX, evt.pageY - canvasY); });
}

function handleWindowResize(evt) {
    findCanvasPosition();
    positionSelector();
}
