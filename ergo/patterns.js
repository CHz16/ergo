// patterns.js
// Original content copyright (c) 1992 Atreid Software SA.
//
// Puzzle layouts copied from Cogito.
//
// { layout | layoutGenerator, substitution? }
//
// layout: A 9x9 grid containing the logical layout of the grid.
// layoutGenerator: A function returning a 9x9 grid.
//
// substitution: If present, this tile substitution will be used instead of a
//               randomly-generated one.

var patterns = [

{ layout: [[0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 1, 1, 1, 0, 0, 0],
           [0, 0, 0, 1, 1, 1, 0, 0, 0],
           [0, 0, 0, 1, 1, 1, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0]] },

{ layout: [[0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1]] },

{ layout: [[0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2]] },

{ layout: [[0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1],
           [2, 2, 2, 2, 2, 2, 2, 2, 2],
           [2, 2, 2, 2, 2, 2, 2, 2, 2],
           [2, 2, 2, 2, 2, 2, 2, 2, 2]] },

{ layout: [[0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 1, 1, 1, 2, 3, 3, 3, 0],
           [0, 1, 1, 1, 2, 3, 3, 3, 0],
           [0, 1, 1, 1, 2, 3, 3, 3, 0],
           [0, 2, 2, 2, 2, 2, 2, 2, 0],
           [0, 4, 4, 4, 2, 5, 5, 5, 0],
           [0, 4, 4, 4, 2, 5, 5, 5, 0],
           [0, 4, 4, 4, 2, 5, 5, 5, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0]],
  substitution: [0, 1, 5, 2, 3, 4] },

{ layout: [[0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2],
           [0, 1, 2, 3, 4, 5, 0, 1, 2]],
  substitution: [0, 1, 2, 3, 4, 5] },

{ layout: [[0, 1, 1, 2, 3, 2, 1, 1, 0],
           [1, 0, 1, 2, 3, 2, 1, 0, 1],
           [1, 1, 0, 2, 4, 2, 0, 1, 1],
           [2, 2, 2, 0, 5, 0, 2, 2, 2],
           [3, 3, 4, 5, 0, 5, 4, 3, 3],
           [2, 2, 2, 0, 5, 0, 2, 2, 2],
           [1, 1, 0, 2, 4, 2, 0, 1, 1],
           [1, 0, 1, 2, 3, 2, 1, 0, 1],
           [0, 1, 1, 2, 3, 2, 1, 1, 0]],
  substitution: [1, 3, 5, 4, 2, 0] },

{ layout: [[0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [0, 0, 0, 1, 1, 1, 2, 2, 2],
           [3, 3, 3, 4, 4, 4, 3, 3, 3],
           [3, 3, 3, 4, 4, 4, 3, 3, 3],
           [3, 3, 3, 4, 4, 4, 3, 3, 3],
           [2, 2, 2, 5, 5, 5, 0, 0, 0],
           [2, 2, 2, 5, 5, 5, 0, 0, 0],
           [2, 2, 2, 5, 5, 5, 0, 0, 0]],
  substitution: [1, 4, 3, 2, 5, 0] },

{ layout: [[0, 0, 0, 0, 0, 0, 0, 0, 0],
           [1, 0, 0, 0, 0, 0, 0, 0, 2],
           [1, 1, 3, 3, 3, 3, 3, 2, 2],
           [1, 1, 3, 0, 0, 0, 3, 2, 2],
           [1, 1, 3, 3, 3, 3, 3, 2, 2],
           [1, 1, 3, 4, 4, 4, 3, 2, 2],
           [1, 1, 3, 3, 3, 3, 3, 2, 2],
           [1, 4, 4, 4, 4, 4, 4, 4, 2],
           [4, 4, 4, 4, 4, 4, 4, 4, 4]],
  substitution: [0, 2, 3, 5, 1] },

{ layoutGenerator: function() {
      var probabilities = [19, 14, 15, 17, 15, 1];
      var runningProbabilities = [0];
      for (var i = 0; i < probabilities.length; i++) {
          runningProbabilities.push(probabilities[i] + runningProbabilities[i]);
      }
      
      var layout = [];
      for (var row = 0; row < gridSize; row++) {
          layout.push([]);
          for (var col = 0; col < gridSize; col++) {
              var rawTile = randRange(0, runningProbabilities[runningProbabilities.length - 1] - 1);
              
              var tileIndex = 0;
              while (rawTile >= runningProbabilities[tileIndex + 1]) {
                  tileIndex++;
              }
              layout[row].push(tileIndex);
          }
      }
      
      return layout;
  },
  substitution: [0, 1, 2, 3, 4, 5] },

];
