// soundeffects.js
// Copyright (c) 2016 'Ili Butterfield. No permission is granted to copy,
// modify, or distribute this code in full or in part without prior written
// permission from the author.
//
// Tiny engine for the loading and playback of sound effects.
//
// This class preloads the list of sounds given in the array sounds. Each
// element of this array represents the data of one sound effect and must be an
// object with the following properties set:
//
//   - key: A key to refer to the sound in this player.
//
//   - urls: An array of files to load. This array should consist of the same
//           sound effect in multiple formats for browser compatibility, so
//           maybe something like ["blah.ogg", "blah.wav"]. This class will load
//           the first file in the array that it guesses the browser can play
//           based on the file extension. If it doesn't think it can play any of
//           them, errorCallback will be called (see below).
//
//  The second optional argument is an object with any or all of the following
//  properties set:
//
//   - base: A base directory for sound URLs. If this property is set, then the
//           sound at sounds[i].url[j] will be fetched from
//           (base + sounds[i].url[j]).
//
//   - minimumDuration: The minimum amount of time, in milliseconds, to play a
//                      sound effect before you can play that sound effect
//                      again. Calling stopAllSounds() will let you disregard
//                      this check. Default = 0.
//
//   - loadCallback: A function to call after any sound is loaded. The function
//                   signature should look like this:
//                       function loadCallback(player, index)
//                   player is the player object, and index is the index of the
//                   sound in sounds that was loaded. This function does not
//                   need a return value, but if it returns true, then no
//                   further sounds will be loaded.
//
//   - errorCallback: A function to call if there's an error in loading a sound.
//                    The function signature should look like this:
//                        function errorCallback(player, index)
//                    player is the player object, and index is the index of the
//                    sound in sounds that failed to load. This function does
//                    not need a return value, but if it returns true, then no
//                    further sounds will be loaded.
//
//   - allCallback: A function to call after all sounds have either loaded or
//                  errored out. The function signature should look like this:
//                      function allCallback(player, soundsHandled, errors)
//                  player is the player object, soundsHandled is the total
//                  number of sounds that were attempted to be loaded, and
//                  errors is an array containing the keys of all the sounds
//                  that were unable to be loaded. This callback will be called
//                  AFTER loadCallback or errorCallback is called for the final
//                  sound to be loaded. If loadCallback or errorCallback returns
//                  true, this callback will be called with soundsHandled and
//                  errors counting only the sounds that were loaded.
function SoundEffectPlayer(sounds, args) {
    this.preloadSound = function(index) {
        if (index == this.sounds.length) {
            this.allCallback(this, index, this.errors);
            return;
        }

        // Find the first sound file that the browser can play.
        var soundUrl = "";
        for (var urlIndex = 0; urlIndex < sounds[index].urls.length; urlIndex++) {
            var url = sounds[index].urls[urlIndex];
            var extension = url.substring(url.lastIndexOf(".") + 1);
            if (this.canPlayType[extension]) {
                soundUrl = url;
                break;
            }
        }

        // If the browser can't play any of them, then error.
        if (soundUrl === "") {
            this.soundErrored(index);
            return;
        }

        // Otherwise, kick off an AJAX request for the sound.
        var player = this;
        var request = new XMLHttpRequest();
        request.open("GET", this.base + soundUrl);
        request.responseType = "arraybuffer";
        request.onload = function(event) { player.soundLoaded(index, event); };
        request.onerror = function(event) { player.soundErrored(index, event); };
        this.statuses[sounds[index].key] = SoundPreloadStatus.loading;
        request.send();
    };

    this.soundLoaded = function(index, event) {
        var player = this;
        this.context.decodeAudioData(event.target.response, function(buffer) {
            var key = player.sounds[index].key;
            player.soundBuffers[key] = buffer;
            player.statuses[key] = SoundPreloadStatus.loaded;

            var stopLoading = player.loadCallback(player, index);
            if (stopLoading) {
                player.allCallback(player, index + 1, player.errors);
                return;
            } else {
                player.preloadSound(index + 1);
            }
        });
    }

    this.soundErrored = function(index, event) {
        var key = this.sounds[index].key;
        this.statuses[key] = SoundPreloadStatus.errored;
        this.errors[this.errors.length] = key;

        var stopLoading = this.errorCallback(this, index);
        if (stopLoading) {
            this.allCallback(this, index + 1, this.errors);
            return;
        } else {
            this.preloadSound(index + 1);
        }
    }

    this.killSoundSource = function(key) {
        this.soundSources[key].stop();
        this.soundSources[key].disconnect();
        delete this.soundSources[key];
        delete this.soundLastPlayed[key];
    }

    this.sounds = sounds;
    args = args || new Object();
    this.base = args.base || "";
    this.minimumDuration = args.minimumDuration || 0;
    this.loadCallback = args.loadCallback || function(player, index) {};
    this.errorCallback = args.errorCallback || function(player, index) {};
    this.allCallback = args.allCallback || function(player, soundsHandled, errors) {};

    this.canPlayType = new Object();
    this.context = args.context || new (window.AudioContext || window.webkitAudioContext)();
    this.context.resume(); // Safari 12 autoplay policy pls
    this.soundBuffers = new Object();
    this.soundSources = new Object();
    this.soundLastPlayed = new Object();
    this.statuses = new Object();
    this.errors = [];

    // First, we check what kinds of formats the browser can and can't play.
    var audioTester = new Audio();
    this.canPlayType["wav"] = (audioTester.canPlayType("audio/wav") !== "" && audioTester.canPlayType("audio/wav") !== "no");
    this.canPlayType["mp3"] = (audioTester.canPlayType("audio/mpeg") !== "" && audioTester.canPlayType("audio/mpeg") !== "no");
    this.canPlayType["ogg"] = (audioTester.canPlayType("audio/ogg") !== "" && audioTester.canPlayType("audio/ogg") !== "no");

    // Now we can kick off preloading.
    this.preloadSound(0);
}
var SoundPreloadStatus = {unloaded: 0, loading: 1, loaded: 2, errored: 3};


// Plays the sound of the given key. If that sound is currently playing, the
// player will stop that sound and start over from the beginning.
SoundEffectPlayer.prototype.playSound = function(key) {
    if (!(key in this.statuses) || (this.statuses[key] != SoundPreloadStatus.loaded)) {
        return;
    }

    var currentTime = Date.now();
    if ((key in this.soundLastPlayed) && ((this.soundLastPlayed[key] + this.minimumDuration) > currentTime)) {
        return;
    }

    if (key in this.soundSources) {
        this.killSoundSource(key);
    }

    var source = this.context.createBufferSource();
    this.soundSources[key] = source;
    this.soundLastPlayed[key] = currentTime;
    source.buffer = this.soundBuffers[key];
    source.connect(this.context.destination);
    source.start();
}


// This one is pretty self-explanatory.
SoundEffectPlayer.prototype.stopAllSounds = function() {
    for (key in this.soundSources) {
        this.killSoundSource(key);
    }
}
