// cookies.js
// Copyright (c) 2010 'Ili Butterfield. No permission is granted to copy,
// modify, or distribute this code in full or in part without prior written
// permission from the author.
//
// Cookie getting/setting utility functions.

// Searches for a cookie set with the key searchKey. Returns the decoded value
// of the cookie if it's found, or null otherwise.
function getCookie(searchKey) {
    var pairs = document.cookie.split("; ");
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("=");
        if (pair[0] == searchKey) return decodeURIComponent(pair[1]);
    }
    return null;
}

// Sets a cookie on the current domain with the given key and value, expiring in
// expiration seconds from the current time.
function setCookie(key, value, expiration) {
    var date = new Date();
    date.setTime(date.getTime() + expiration);
    var sld = document.domain.split(".").slice(-2).join(".");
    document.cookie = key + "=" + encodeURIComponent(value) + "; domain=" + document.domain + "; path=/;  expires=" + date.toGMTString();
}
