// rulesets.js
// Original content copyright (c) 1992 Atreid Software SA.
//
// Arrow rule behavior copied from Cogito.
//
//           side 3
//     8 7 6 5 4 3 2 1 0
//    0                 8
//    1                 7
// s  2                 6 s
// i  3                 5 i
// d  4                 4 d
// e  5                 3 e
//    6                 2
// 0  7                 1 2
//    8                 0
//     0 1 2 3 4 5 6 7 8
//           side 1

// { actionGenerator, deactivatedButtons?, scrambleAmount }
//
// actionGenerator: A function(side, arrow) returning a list of [side delta,
//                  arrow delta, shift amount] to be shifted by clicking on
//                  the arrow on that side.
//
// deactivatedButtons: If present, a list of [side, arrow] to be deactivated.
//
// scrambleAmount: Number of moves to be used to scramble the grid.

var rulesets = [

{ actionGenerator: function(side, arrow) { return [[side, arrow, 1]]; },
  scrambleAmount: 35 },

{ actionGenerator: function(side, arrow) { return [[side, arrow, 2]]; },
  scrambleAmount: 35 },

{ actionGenerator: function(side, arrow) { if (side % 2 == 0) return [[side + 1, arrow, 1]]; else return [[side - 1, arrow, 1]]; },
  scrambleAmount: 80 },

{ deactivatedButtons: [[0, 1], [0, 2], [0, 6], [1, 2], [1, 6], [2, 2], [2, 6], [2, 7], [3, 2], [3, 6]],
  actionGenerator: function(side, arrow) { return [[side, arrow, 1]]; },
  scrambleAmount: 60 },

{ actionGenerator: function(side, arrow) { return [[side, arrow, -1]]; },
  scrambleAmount: 80 },

{ actionGenerator: function(side, arrow) { return [[side + 2, arrow, 1]]; },
  scrambleAmount: 60 },

{ actionGenerator: function(side, arrow) { return [[side, arrow, 1], [side + 2, arrow, 1]]; },
  scrambleAmount: 60 },

{ actionGenerator: function(side, arrow) { if (side % 2 == 0) return [[side, arrow, 1], [side + 1, arrow, 1]]; else return [[side, arrow, 1], [side - 1, arrow, 1]]; },
  scrambleAmount: 60 },

{ actionGenerator: function(side, arrow) { return [[side, arrow, 1], [side + 2, arrow, -1]]; },
  scrambleAmount: 35 },

{ deactivatedButtons: [[0, 1], [0, 2], [0, 6], [1, 2], [1, 6], [2, 2], [2, 6], [2, 7], [3, 2], [3, 6]],
  actionGenerator: function(side, arrow) { if (side % 2 == 0) return [[side, arrow, 1], [side + 1, arrow, 1]]; else if ((side == 1 && arrow == 1) || (side == 3 && arrow == 7)) return [[side, arrow, 1]]; else return [[side, arrow, 1], [side - 1, arrow, 1]]; },
  scrambleAmount: 500 },

{ actionGenerator: function(side, arrow) { if (side < 2) return [[side, arrow, 1], [side, arrow + 1, 1]]; else return [[side, arrow, 1], [side, arrow - 1, 1]]; },
  scrambleAmount: 35 },

{ actionGenerator: function(side, arrow) { if (side < 2) return [[side, arrow, 1], [side, arrow - 2, 1]]; else return [[side, arrow, 1], [side, arrow + 2, 1]]; },
  scrambleAmount: 30 },

];