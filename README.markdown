Ergo
====

http://quiteajolt.com/projects/ergo/

Basically just a JavaScript port of the old puzzle game *Cogito*.

Game code & assets are in the `ergo/` subdirectory. The files in the top level directory are just some utility files and the webpage source for my static site generator.
